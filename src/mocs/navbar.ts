export type NavItem = {
  transKey: string;
  link: string;
  onClick?: (e?: React.SyntheticEvent) => void;
};

export const navbar: NavItem[] = [
  {
    transKey: "menu1",
    link: "#",
  },
  {
    transKey: "menu2",
    link: "#",
  },
  {
    transKey: "menu3",
    link: "#",
  },
  {
    transKey: "menu4",
    link: "#",
  },
  {
    transKey: "menu5",
    link: "#",
  },
];
