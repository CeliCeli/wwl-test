import { FC } from "react";
import { ScInput } from "./styled";

import { v4 as uuid } from "uuid";

export enum InputTypes {
  Text = "text",
  Number = "number",
  Tel = "tel",
  Email = "email",
  File = "file",
}

export interface InputProps {
  type?: InputTypes;
  name?: string;
  placeholder?: string | JSX.Element;
  value?: string | number;
  id?: string;
  readonly?: boolean;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  className?: string;
}

export const Input: FC<InputProps> = ({
  type = InputTypes.Text,
  placeholder,
  value,
  id: propsId,
  readonly,
  onBlur,
  onChange,
  className,
}) => {
  const id = propsId ?? uuid();

  return (
    <ScInput className={className || ""}>
      <input
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        type={type}
        id={id}
        readOnly={readonly}
      />
      {placeholder && String(value).length < 1 && (
        <div className="placeholder">{placeholder}</div>
      )}
    </ScInput>
  );
};
