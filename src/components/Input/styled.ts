import styled from "styled-components/macro";
import { theme } from "theme";

export const ScInput = styled.div`
  padding: 2px;
  border-radius: 60px;
  display: inline-block;
  box-sizing: border-box;
  position: relative;

  background: linear-gradient(
    133.44deg,
    rgba(0, 0, 0, 0.4) 2.27%,
    rgba(36, 36, 36, 0.4) 93.62%
  );
  box-shadow: -53.1479px -53.1479px 106.296px rgba(51, 51, 51, 0.4),
    17.716px 17.716px 70.8639px rgba(0, 0, 0, 0.6);

  .placeholder {
    color: rgba(195, 193, 193, 1);
    position: absolute;
    left: 50%;
    top: 50%;
    font-size: 14px;
    translate: -50% -50%;
    pointer-events: none;
    white-space: nowrap;

    span {
      color: ${theme.color.primary};
    }
  }

  input {
    padding: 0 16px;
    text-align: center;
    height: 52px;
    border-radius: 60px;
    box-sizing: border-box;
    color: ${theme.color.white};
    font-size: 16px;
    line-height: 21px;
    background: linear-gradient(
        318.96deg,
        rgba(0, 0, 0, 0.4) -1.9%,
        rgba(36, 36, 36, 0.4) 105%
      ),
      #272727;
    border: none;
    outline: none;
    width: 100%;
  }
`;
