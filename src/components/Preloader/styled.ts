import { styled, css } from "styled-components/macro";
import { theme } from "../../theme";
import { hexToRGBA } from "../../helpers/hexToRgba";

interface ScPreloaderProps {
  progress: number;
  svgcolor: string;
  isfinish: boolean;
}
export const ScPreloader = styled.div<ScPreloaderProps>`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 9999;
  background-color: ${hexToRGBA(theme.color.secondary, 1)};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  pointer-events: none;
  transition: ease 0.2s;
  opacity: ${({ isfinish }) => (isfinish ? 0 : 1)};
  svg {
    max-width: 260px;
    margin-bottom: 32px;
  }
  .bar {
    width: 300px;
    height: 4px;
    position: relative;

    &::before {
      top: 0;
      left: 0;
      right: ${({ progress }) => ` calc(100% - ${progress}%)`};
      bottom: 0;
      position: absolute;
      content: "";
      background-color: ${theme.color.primary};
      border-radius: 4px;
    }
  }

  ${({ progress, svgcolor }) =>
    progress &&
    css`
      svg path {
        fill: ${`rgb(${svgcolor})`};
      }
    `}
`;
