import React, { useState, useEffect } from "react";
import { delay } from "helpers/delay";

import { ScPreloader } from "./styled";
import { ReactComponent as Logo } from "assets/logo_loader.svg";
import { useLoadingRoute } from "hooks/useLoadingRoute";

interface PreloaderProps {
  onFinish: () => void;
}

const color1 = [234, 57, 70];
const color2 = [230, 187, 39];

const calcColor = (step: number) => {
  const r = Math.round(color1[0] + (color2[0] - color1[0]) * (step / 100));
  const g = Math.round(color1[1] + (color2[1] - color1[1]) * (step / 100));
  const b = Math.round(color1[2] + (color2[2] - color1[2]) * (step / 100));
  return [r, g, b];
};

export const Preloader: React.FC<PreloaderProps> = ({ onFinish }) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isFinish, setIsFinish] = useState<boolean>(false);
  const [progress, setProgress] = useState<number>(1);

  const loadingRoute = useLoadingRoute();

  const interval = (maxNum: number, timeout: number) => {
    return setInterval(() => {
      setProgress((s) => (s < maxNum ? ++s : s));
    }, timeout);
  };

  const handleLoad = async () => {
    const animationInterval = interval(100, 1);
    await delay(800);
    clearInterval(animationInterval);
    setIsFinish(true);
    await delay(300);
    onFinish();
    await delay(300);
    setIsLoading((s) => !s);
  };

  useEffect(() => {
    const progressInterval = interval(90, 10);

    if (!loadingRoute && progress === 90) {
      handleLoad();
    }

    return () => {
      clearInterval(progressInterval);
    };
  }, [loadingRoute, progress, onFinish]);

  return (
    (isLoading && (
      <ScPreloader
        svgcolor={calcColor(progress).toString()}
        progress={progress}
        isfinish={isFinish}
      >
        <Logo />
        <div className="bar"></div>
      </ScPreloader>
    )) ||
    null
  );
};
