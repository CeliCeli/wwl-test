import { FC, ReactNode } from "react";
import { ButtonSize, ButtonTheme, ScButton, ScButtonProps } from "./styled";

interface ButtonProps extends ScButtonProps {
  className?: string;
  onClick?: (e?: React.SyntheticEvent) => void;
  children?: ReactNode;
}
export const Button: FC<ButtonProps> = ({
  colorTheme = ButtonTheme.Secondary,
  size = ButtonSize.M,
  onClick,
  children,
  className,
}) => {
  return (
    <ScButton
      size={size}
      className={className}
      colorTheme={colorTheme}
      onClick={onClick}
    >
      <div className="buttonContent">{children}</div>
    </ScButton>
  );
};
