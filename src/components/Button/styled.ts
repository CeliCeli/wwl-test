import styled, { css, DefaultTheme } from "styled-components/macro";
import { theme } from "theme";

export enum ButtonTheme {
  Primary = "primary",
  Secondary = "secondary",
}

export enum ButtonSize {
  M,
  L,
}

export interface ScButtonProps {
  colorTheme?: ButtonTheme;
  size?: ButtonSize;
}

export const ScButton = styled.button<ScButtonProps>`
  border-radius: 100px;
  border: none;
  cursor: pointer;
  padding: 4px 4px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.4);
  transition: all 0.2s;
  box-sizing: border-box;
  .buttonContent {
    border-radius: 100px;
    box-shadow: -8.16964px -8.16964px 16.3393px rgba(18, 18, 18, 0.4),
      8.16964px 8.16964px 24.5089px rgba(0, 0, 0, 0.5);
    font-weight: 700;
    transition: all 0.2s;
    box-sizing: border-box;
    height: 100%;

    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
  }

  ${({ colorTheme }) => {
    switch (colorTheme) {
      case ButtonTheme.Primary:
        return css`
          background: linear-gradient(112.3deg, #bc333c 68.24%, #681217 100%);

          .buttonContent {
            background: linear-gradient(
              134.43deg,
              #681217 14.99%,
              #bc333c 82.61%
            );
            color: ${theme.color.white};
          }
        `;
      default:
        return css`
          background: ${theme.effect.secondaryGradientInvert};

          .buttonContent {
            background: ${theme.effect.secondaryGraient};
            color: ${theme.color.lightGray};
            text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.4);
          }
          &:hover {
            .buttonContent {
              color: ${theme.color.white};
            }
          }
        `;
    }
  }}
  ${({ size }) => {
    switch (size) {
      case ButtonSize.L:
        return css`
          height: 75px;
          .buttonContent {
            padding: 0 16px;
            font-size: 18px;
          }
        `;
      default:
        return css`
          height: 47px;
          .buttonContent {
            font-size: 16px;
            padding: 0 14px;
          }
        `;
    }
  }}
  &:hover {
    box-shadow: 0px 2px 16px 0px rgba(235, 56, 70, 0.62);
  }
`;
