import React from "react";

interface CustomCursorProps {
  x: number;
  y: number;
}

export const Cursor: React.FC<CustomCursorProps> = ({ x, y }) => (
  <div className={`cursor`} style={{ left: x, top: y }} />
);
