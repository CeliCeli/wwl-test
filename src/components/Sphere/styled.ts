import { hexToRGBA } from "helpers/hexToRgba";
import styled, { css, keyframes } from "styled-components/macro";
import { theme } from "theme";

const changeStroke = keyframes`
  0%,100%,50%{
    stroke: #2f2f2f;
    scale: 1;
    opacity: 1;
  }
  20%{
    stroke: ${theme.color.primary};
    scale: 1.03;
    opacity: 1;
  }
`;

const init = keyframes`
  0%{
    stroke: transparent;
    fill: ${hexToRGBA(theme.color.primary, 0.1)};
    opacity: 0;
  }
  80%{
    opacity: 1;
  }
  100%{
    stroke: #2f2f2f;
    fill: transparent;
    opacity: 1;
  }
`;

export const ScSphere = styled.div<{ totalPath: number }>`
  path {
    stroke: transparent;
    opacity: 0;
    fill: ${hexToRGBA(theme.color.primary, 0.1)};
    animation: ${init} 1s ease-in forwards,
      ${changeStroke} 6s 1.1s ease infinite;
  }
  ${({ totalPath }) => {
    let styles = "";

    for (let i = 0; i < totalPath; i++) {
      const index = i + 1;
      const indexString = index.toString();
      const sek =
        indexString.length === 1
          ? `0.${index}`
          : `${indexString[0]}.${indexString[1]}`;
      styles += `
      path:nth-child(${totalPath - i - 1}){
        animation-delay: ${Number(sek) * 3 + 1.1}s;
        
      }
      `;
    }

    return css`
      ${styles}
    `;
  }}
`;
