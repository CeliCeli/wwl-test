import React, { useState, useEffect, useRef } from "react";
import { ReactComponent as SphereSVG } from "assets/sphere.svg";
import { ScSphere } from "./styled";

export const Sphere: React.FC = () => {
  const svgRef = useRef<SVGSVGElement>(null);
  const [totalPath, setTotalPath] = useState(0);

  useEffect(() => {
    const paths = svgRef.current?.getElementsByTagName("path");
    const numberOfPaths = paths?.length ?? 0;
    setTotalPath(numberOfPaths);
  }, []);

  return (
    <ScSphere totalPath={totalPath}>
      <SphereSVG ref={svgRef} />
    </ScSphere>
  );
};
