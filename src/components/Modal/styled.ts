import { TPosition } from "context/modal";
import { hexToRGBA } from "helpers/hexToRgba";
import styled, { css, keyframes } from "styled-components";
import { media } from "styles/sizes";
import { theme } from "theme";

const hide = keyframes`
  to{
    opacity: 0;
  }
`;

const show = keyframes`
  to{
   opacity: 1;
  }
`;

interface ScModalBodyProps {
  isModal?: boolean;
  isClosed?: boolean;
}

export const ScModalBody = styled.div<ScModalBodyProps>`
  position: relative;
  z-index: 999;
  position: fixed;
  right: 0;
  color: #fff;
  top: 50%;
  transition: 0.1s;
  transform: translate(200%, -50%);
  opacity: 0;

  & ~ .backdrop {
    position: fixed;
    right: 0;
    top: 0;
    bottom: 0;
    left: 0;
    background-color: ${hexToRGBA(theme.color.secondary, 0.9)};
    z-index: 998;
    opacity: 0;
    transition: 0.1s;
  }

  @media (${media.tablet}) {
    top: 0;
    bottom: 0;
    transform: translate(200%, 0);
  }

  ${({ isModal }) =>
    isModal &&
    css`
      transform: translate(0, -50%);
      animation: ${show} 0.1s ease forwards;
      @media (${media.tablet}) {
        top: 0;
        transform: translate(0, 0);
      }

      & ~ .backdrop {
        animation: ${show} 0.1s ease forwards;
      }
    `}

  ${({ isClosed }) =>
    isClosed &&
    css`
      transform: translate(200%, -50%);
      @media (${media.tablet}) {
        top: 0;
        transform: translate(200%, 0);
      }
      & ~ .backdrop {
        animation: ${hide} 0.58s ease forwards;
      }
    `}
`;

export const ScModalContent = styled.div<{ buttonPosition: TPosition }>`
  padding: 26px 26px 26px 26px;
  background: linear-gradient(0deg, #191919, #191919), #343434;
  box-shadow: 12px 12px 72px rgba(0, 0, 0, 0.4);
  border-radius: 36px 0px 0px 36px;
  border: 1px solid rgba(25, 25, 25, 1);
  border-right: none;

  .close {
    position: absolute;
    padding: 0;
    background: transparent;
    height: auto;
    right: 26px;
    .buttonContent {
      display: block;
      padding: 0;
      height: 24px;
      width: 24px;
      border-radius: 8px;
      border: none;
      background: ${theme.color.primary};
      position: relative;

      &:before,
      &:after {
        position: absolute;
        content: "";
        left: 50%;
        top: 50%;
        translate: -50% -50%;
        width: 9px;
        height: 2px;
        background-color: ${theme.color.secondary};
        rotate: 45deg;
      }
      &:after {
        rotate: -45deg;
      }
    }
  }

  ${({ buttonPosition }) => {
    switch (buttonPosition) {
      case "top":
        return css`
          padding: 76px 26px 26px 26px;

          .close {
            top: 25px;
          }
        `;
      case "bottom":
        return css`
          padding: 26px 26px 76px 26px;

          .close {
            bottom: 25px;
          }
        `;
    }
  }}

  @media (${media.tablet}) {
    position: relative;
    height: 100%;
    box-sizing: border-box;
    & > * {
      height: 100%;
      box-sizing: border-box;
    }
  }
`;
