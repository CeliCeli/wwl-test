import { ModalContext } from "context/modal";
import { FC, useContext, useState } from "react";
import { ScModalBody, ScModalContent } from "./styled";
import { delay } from "helpers/delay";
import { Button } from "components/Button";

export const Modal: FC = () => {
  const [isClosed, setIsClodes] = useState(false);
  const {
    isModal,
    modalContent: children,
    setModal,
    buttonPosition,
  } = useContext(ModalContext);

  const handleCloseModal = async () => {
    if (isClosed) return;
    setIsClodes(true);
    await delay(600);
    setModal();
    setIsClodes(false);
  };

  return (
    <>
      <ScModalBody isClosed={isClosed} isModal={isModal}>
        <ScModalContent buttonPosition={buttonPosition}>
          {isModal && (
            <>
              {children}
              {buttonPosition !== null && (
                <Button className="close" onClick={handleCloseModal} />
              )}
            </>
          )}
        </ScModalContent>
      </ScModalBody>
      {isModal && <div className="backdrop" onClick={handleCloseModal}></div>}
    </>
  );
};
