import React, { FC, ReactElement, Suspense, useEffect, useState } from "react";
import { GlobalStyle } from "./styles/global";
import { LandingLayout } from "./modules/Layout";

import CombineProviders from "./helpers/combineProviders";
import { Preloader } from "./components/Preloader";
import { Cursor } from "./components/Cursor";
import { LocalesProvider } from "context/locales";
import { ModalProvider } from "context/modal";
import { useCurrentMediaType } from "hooks/useCurrentMediaType";
import { Device } from "styles/sizes";
import { Header } from "modules/Landing/Header";

const providers = [LocalesProvider, ModalProvider];

const App: FC = (): ReactElement => {
  const [isContentReady, setIsContentReady] = useState<boolean>(false);
  const [cursorPosition, setCursorPosition] = useState({ x: 0, y: 0 });
  const isTablet = useCurrentMediaType(Device.TABLET);

  const handleMouseMove = (event: MouseEvent) => {
    const { clientX, clientY } = event;
    setCursorPosition({ x: clientX, y: clientY });
  };

  useEffect(() => {
    document.addEventListener("mousemove", handleMouseMove);

    return () => {
      document.removeEventListener("mousemove", handleMouseMove);
    };
  }, []);
  return (
    <>
      <CombineProviders components={[...providers]}>
        <GlobalStyle />
        {!isTablet && <Cursor x={cursorPosition.x} y={cursorPosition.y} />}
        <Preloader onFinish={() => setIsContentReady(true)} />
        {isContentReady && <LandingLayout children={undefined} />}
      </CombineProviders>
    </>
  );
};

export default App;
