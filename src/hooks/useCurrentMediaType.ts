import { useEffect, useState } from "react";
import { Device } from "styles/sizes";

export const useCurrentMediaType = (device: Device): boolean => {
  const [isMatch, setIsMatch] = useState<boolean>(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMatch(window.innerWidth <= device);
    };

    handleResize();
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [device]);

  return isMatch;
};
