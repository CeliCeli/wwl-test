import React, { FC, ReactNode, useMemo, useState, createContext } from "react";
import { useTranslation } from "react-i18next";

export enum Locales {
  EN = "en",
  UA = "ua",
}

const DEAFULT_LOCALE = Locales.EN;

interface LocalesContextProps {
  currentLocale: Locales;
  swhitchLocale: (locale?: Locales) => void;
}

export const LocalesContext = createContext<LocalesContextProps>({
  currentLocale: DEAFULT_LOCALE,
  swhitchLocale: () => {},
});

export const LocalesProvider: FC<{ children?: ReactNode }> = ({ children }) => {
  const [currentLocale, setCurrentLocale] = useState<Locales>(DEAFULT_LOCALE);
  const { i18n } = useTranslation();

  const swhitchLocale = (locale?: Locales) => {
    const newLocale =
      locale ??
      (currentLocale === DEAFULT_LOCALE ? Locales.UA : DEAFULT_LOCALE);

    setCurrentLocale(newLocale);
    i18n.changeLanguage(newLocale);
  };

  const value = useMemo(
    () => ({ currentLocale, swhitchLocale }),
    [currentLocale]
  );
  return (
    <LocalesContext.Provider value={value}>{children}</LocalesContext.Provider>
  );
};
