import React, {
  FC,
  ReactNode,
  useMemo,
  useState,
  createContext,
  useEffect,
} from "react";

export type TPosition = "top" | "bottom" | null;

interface ModalContextProps {
  setModal: (content?: ReactNode) => void;
  isModal: boolean;
  modalContent: ReactNode | undefined;
  buttonPosition: TPosition;
  setButtonPosition: (position: TPosition) => void;
}

export const ModalContext = createContext<ModalContextProps>({
  setModal: () => {},
  isModal: false,
  modalContent: undefined,
  buttonPosition: "top",
  setButtonPosition: () => {},
});

export const ModalProvider: FC<{ children?: ReactNode }> = ({ children }) => {
  const [isModal, setIsModal] = useState<boolean>(false);
  const [modalContent, setModalContent] = useState<ReactNode | undefined>(
    undefined
  );
  const [buttonPositionState, setButtonPositionState] =
    useState<TPosition>("top");

  const setModal = (content?: ReactNode) => {
    setIsModal(!!content);
    setModalContent(content);
  };

  const setButtonPosition = (position: TPosition) => {
    setButtonPositionState(position);
  };

  useEffect(() => {
    const body = document.querySelector("body");
    if (isModal) {
      body?.classList.add("is-open");
    } else {
      body?.classList.remove("is-open");
    }
  }, [isModal]);

  const value = useMemo(
    () => ({
      isModal,
      setModal,
      modalContent,
      buttonPosition: buttonPositionState,
      setButtonPosition,
    }),
    [isModal, setModal, modalContent, buttonPositionState, setButtonPosition]
  );

  return (
    <ModalContext.Provider value={value}>{children}</ModalContext.Provider>
  );
};
