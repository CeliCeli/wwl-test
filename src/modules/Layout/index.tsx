import { Modal } from "components/Modal";
import { Header } from "modules/Landing/Header";
import { FC, ReactNode } from "react";

export const LandingLayout: FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <>
      <Header />
      {children}
      <Modal />
    </>
  );
};
