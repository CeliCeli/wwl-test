import { FC } from "react";

export const NotFoundPage: FC = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <h1>404</h1>
      <span>some error layout there</span>
    </div>
  );
};
