import React, { Component, ErrorInfo, ReactNode } from "react";
import { NotFoundPage } from "./NotFoundPage";

interface Props {
  children?: ReactNode;
}

interface State {
  hasError: boolean;
}

class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false,
  };

  public static getDerivedStateFromError(_: Error): State {
    return { hasError: true };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    console.error("Uncaught error:", error, errorInfo);
  }

  public render(): ReactNode {
    if (this.state.hasError) {
      return <NotFoundPage />;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
