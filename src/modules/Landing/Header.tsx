import { FC, useContext } from "react";
import { ScHeader } from "./styled";
import { ScContainer } from "styles";

import { Button } from "components/Button";
import { ButtonSize, ButtonTheme } from "components/Button/styled";
import { Sphere } from "components/Sphere";
import { useTranslation } from "react-i18next";
import { LocalesContext } from "context/locales";
import { ReactComponent as Call } from "assets/call.svg";
import { ReactComponent as Logo } from "assets/logo.svg";
import { ModalContext } from "context/modal";
import { ModalContacts, ModalForm, ModalMenu } from "modules/Modals";
import { navbar } from "mocs/navbar";
import { Device } from "styles/sizes";
import { useCurrentMediaType } from "hooks/useCurrentMediaType";

export const Header: FC = () => {
  const { t } = useTranslation("layout");
  const { currentLocale, swhitchLocale } = useContext(LocalesContext);
  const { setModal, isModal, setButtonPosition } = useContext(ModalContext);
  const isMobile = useCurrentMediaType(Device.MOBILE);
  const isMoileS = useCurrentMediaType(Device.MOBILE_S);

  const handleChangeLocale = () => {
    swhitchLocale();
  };

  const handleOpenMenu = () => {
    setModal(<ModalMenu />);
    setButtonPosition(null);
  };

  const handleOpenContects = () => {
    setModal(<ModalContacts />);
    setButtonPosition("bottom");
  };

  const handleOpenFrom = () => {
    setModal(<ModalForm />);
    setButtonPosition("top");
  };

  return (
    <ScHeader>
      <ScContainer>
        <div className="headTop">
          <a className="logo" href={"/"}>
            <Logo />
          </a>
          <Button
            size={ButtonSize.L}
            className={`hamburger`}
            onClick={handleOpenMenu}
          >
            {Array(4)
              .fill(null)
              .map((_, i) => (
                <span key={i} />
              ))}
          </Button>
          <div className="asideControll">
            <Button colorTheme={ButtonTheme.Primary} onClick={handleOpenFrom}>
              {t("consultation")}
            </Button>
            {!isMoileS && (
              <Button onClick={handleChangeLocale}>
                {currentLocale.toUpperCase()}
              </Button>
            )}
          </div>
        </div>
        <div className="headMdl">
          {!isMobile && (
            <nav>
              {navbar.map(({ transKey, link }, index) => (
                <a href={link} key={index}>
                  {t(`menu.${transKey}`)}
                </a>
              ))}
            </nav>
          )}
          <h1 dangerouslySetInnerHTML={{ __html: t("headline") as string }} />
          {!isMobile && (
            <Button onClick={handleOpenContects}>
              <Call />
              {t("callback")}
            </Button>
          )}
        </div>
      </ScContainer>
      <Sphere />
    </ScHeader>
  );
};
