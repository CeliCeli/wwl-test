import styled from "styled-components/macro";
import { theme } from "theme";
import { media } from "styles/sizes";
import { hexToRGBA } from "helpers/hexToRgba";
import { ScButton } from "components/Button/styled";
import { ScSphere } from "components/Sphere/styled";
import { keyframes } from "styled-components";

const show = keyframes`
  to{
    opacity: 1;
    scale: 1
  }
`;
const showAside = keyframes`
  to{
    opacity: 1;
    translate: 0 0 ;
    scale: 1
  }
`;

export const ScHeader = styled.header`
  min-height: 800px;
  box-sizing: border-box;
  position: relative;
  background-color: ${theme.color.secondary};
  z-index: 1;
  margin-bottom: 100vh;
  overflow: hidden;

  .headTop {
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: relative;
    padding: 42px 0;
    z-index: 2;
  }

  .hamburger {
    position: absolute;
    top: 50%;
    left: 50%;
    translate: -50% -50%;
    z-index: 999;
    .buttonContent {
      position: relative;
      width: 67px;
    }
    span {
      position: absolute;
      display: block;
      height: 4px;
      left: 0;
      right: 0;
      background-color: #dadada;
      border-radius: 3px;
      transition: 0.2s;
      &:nth-child(1) {
        top: 21px;
        left: 26px;
        right: 26px;
      }
      &:nth-child(2),
      &:nth-child(3) {
        top: 32px;
        left: 18px;
        right: 18px;
      }

      &:nth-child(4) {
        top: 43px;
        left: 26px;
        right: 26px;
      }
    }
  }

  .asideControll {
    button {
      margin-left: 46px;
      &:first-child {
        width: 166px;
        margin-left: 0;
      }
      &:last-child {
        & > div {
          padding: 0;
          width: 4.5ch;
        }
      }
    }
  }

  &:before {
    position: absolute;
    content: "";
    left: 0;
    right: -100vw;
    bottom: 0;
    height: 600px;
    background-color: #1b1b1b;
    z-index: -1;
    transform: rotate(5deg);
  }

  &:after {
    position: absolute;
    content: "";
    left: -200px;
    right: -200px;
    bottom: 11%;
    top: 0;
    background: linear-gradient(165.63deg, #171717 54.22%, #000000 120.11%);
    z-index: -1;
    transform: rotate(-5deg);
  }

  ${ScSphere} {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    opacity: 0.4;
    width: 600px;
    height: auto;
    max-width: 100%;

    svg {
      width: 100%;
      height: 100%;
    }
  }

  .headMdl {
    position: relative;
    z-index: 3;
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    align-items: center;
    min-height: 600px;

    ${ScButton} {
      position: absolute;
      height: auto;
      border-radius: 30px;
      padding: 2px;
      right: 0;
      background: linear-gradient(137.77deg, #050505 20.65%, #181818 82.66%);
      translate: 200% 0;
      scale: 0.5;
      opacity: 0;
      animation: ${showAside} 0.7s ease forwards;

      .buttonContent {
        height: auto;
        display: flex;
        flex-direction: column;
        border-radius: 30px;
        box-sizing: border-box;
        padding: 26px 32px 22px;
        width: 124px;
        background: linear-gradient(143.15deg, #171717 32%, #111111 92.5%);
        box-shadow: -53.1479px -53.1479px 106.296px rgba(51, 51, 51, 0.4),
          17.716px 17.716px 70.8639px rgba(0, 0, 0, 0.6);
        font-weight: 400;
        font-size: 10px;
        line-height: 13px;
        color: ${theme.color.lightGray};
        position: relative;
        &::before {
          position: absolute;
          content: "";
          z-index: -1;
          top: -5px;
          right: -5px;
          left: 30%;
          bottom: 30%;
          border-radius: 3px 30px 3px 3px;
          background: linear-gradient(
            177.13deg,
            #b02833 0%,
            #6c141b 44.78%,
            #d0f34d 57.29%,
            #6c7b34 99.13%
          );
          background-size: 100% 220%;
          background-position: 0 0;
          box-shadow: 5px -5px 40px ${hexToRGBA(theme.color.primary, 0.3)};
          transition: ease-out 0.2s;
        }
      }

      &:hover {
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.4);
        .buttonContent:before {
          background-position: 0 100%;
          box-shadow: 10px -10px 42px ${hexToRGBA("#d0f34d", 0.5)};
        }
      }
      svg {
        margin-bottom: 13px;
      }
    }
  }

  h1 {
    margin: 0 auto 64px auto;
    scale: 0.5;
    opacity: 0;
    animation: ${show} 0.7s ease forwards;
  }

  nav {
    display: flex;
    justify-content: space-around;
    flex-direction: column;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    translate: -200% 0;
    scale: 0.5;
    opacity: 0;
    animation: ${showAside} 0.7s ease forwards;

    a {
      rotate: -90deg;
      scale: 1 1;
      margin: 0;
      font-weight: 400;
      font-size: 14px;
      line-height: 100px;
      text-align: center;
      letter-spacing: 0.1em;
      text-transform: uppercase;
    }
  }

  @media (max-width: 1500px) {
    .headMdl {
      min-height: 500px;
    }
    h1 {
      font-size: 48px;
    }
  }

  @media (${media.mobileS}) {
    min-height: 0;
    overflow: initial;
    .headTop {
      padding: 32px 0;
    }
    .hamburger {
      border-radius: 8px;
      position: static;
      translate: 0;
      order: 2;
      & > div {
        border-radius: 6px;
      }
    }

    .logo {
      max-width: 140px;

      svg {
        width: 100%;
      }
    }

    .asideControll {
      flex: 1 1 auto;
      padding: 0 16px;
      button:last-child {
        width: 100%;
        & > div {
          width: 100%;
        }
      }
    }
    .headMdl {
      min-height: 0;
      padding: 80px 0;
    }

    &:before {
      content: none;
    }
    &:after {
      transform: none;
      left: 0;
      bottom: 0;
      right: 0;
    }
  }
  @media (${media.mobileXS}) {
    h1 {
      font-size: 32px;
    }
  }
`;
