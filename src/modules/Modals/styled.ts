import { ScInput } from "components/Input/styled";
import styled from "styled-components/macro";
import { media } from "styles/sizes";
import { theme } from "theme";

export const ScContactsModal = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 16px;
  button {
    height: auto;
    width: auto;
    border-radius: 30px;
    flex: 0 0 calc(50% - 8px);
    max-width: calc(50% - 8px);
    box-sizing: border-box;
    .buttonContent {
      background: none;
      box-shadow: none;
      padding: 0;
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 0;
      height: 108px;
    }

    img {
      width: 42px;
      height: 42px;
      vertical-align: middle;
    }
  }
`;

export const ScMenuModal = styled.div`
  padding: 120px 140px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  z-index: 1;
  overflow: hidden;
  .background {
    position: absolute;
    z-index: 1;
    font-weight: 900;
    font-size: 100px;
    line-height: 128px;
    letter-spacing: 0.3em;
    color: #212121;
    top: 50%;
    translate: 0 -50%;
    &:first-child {
      left: 20px;
    }
    &:nth-child(2) {
      right: -135px;
    }
  }

  nav {
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin-bottom: 55px;
    position: relative;
    z-index: 2;
    a {
      display: flex;
      color: ${theme.color.white};
      font-weight: 900;
      font-size: 35px;
      line-height: 45px;
      letter-spacing: 0.1em;
      align-items: baseline;
      text-transform: uppercase;
      text-align: left;
      margin-bottom: 35px;
      transition: 0.2s;
      &:hover {
        opacity: 0.6;
      }
      span {
        font-weight: 400;
        font-size: 16px;
        line-height: 21px;
        letter-spacing: 0.1em;
        margin-right: 20px;
      }
      &:nth-child(2n) {
        color: ${theme.color.primary};
      }
    }
  }

  .media {
    display: flex;
    justify-content: flex-start;
    position: relative;
    z-index: 2;

    button {
      margin-right: 22px;
      border-radius: 8px;
      padding: 2px;
      height: auto;
      box-shadow: none;

      .buttonContent {
        border-radius: 5px;
        padding: 0;
        height: auto;
        display: flex;
        align-items: center;
        justify-content: center;
        line-height: 1;
        width: 36px;
        height: 36px;
        box-shadow: none;
      }
      svg {
        vertical-align: middle;
        height: 24px;
        width: 24px;
        path {
          transition: 0.2s;
        }
      }
      &:hover {
        svg path {
          fill: ${theme.color.primary};
        }
      }
    }
  }
  @media (${media.tablet}) {
    padding-top: 0;
    padding-bottom: 0;

    padding: 0 32px;
    overflow: auto;
    .background {
      font-size: 60px;
      line-height: 60px;
    }
    nav {
      margin-bottom: 0;
      a {
        margin-bottom: 16px;
        font-size: 24px;
      }
    }
  }
`;

export const ScModalForm = styled.div`
  padding: 80px 50px 120px 50px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;

  .headline {
    font-weight: 900;
    font-size: 24px;
    line-height: 35px;
    max-width: 292px;
    margin-bottom: 40px;
  }

  ${ScInput} {
    width: 100%;
    max-width: 350px;
    margin-bottom: 8px;
    &:last-of-type {
      margin-bottom: 22px;
    }
  }

  button {
    height: 56px;
    width: 100%;
  }
  @media (${media.tablet}) {
    height: 100%;
    padding: 0 16px;
    overflow-y: auto;
  }
`;
