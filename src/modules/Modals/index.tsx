import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { ScContactsModal, ScModalForm, ScMenuModal } from "./styled";
import { navbar } from "mocs/navbar";

import { ReactComponent as Fb } from "assets/fb.svg";
import { ReactComponent as Insta } from "assets/insta.svg";
import { ReactComponent as LinkedIn } from "assets/linkedin.svg";
import { Button } from "components/Button";

import wp from "assets/whatsapp.png";
import tg from "assets/telegram.png";
import viber from "assets/viber.png";
import mail from "assets/main.png";
import { Input } from "components/Input";
import { ButtonTheme } from "components/Button/styled";

export const ModalMenu: FC = () => {
  const { t } = useTranslation("layout");

  return (
    <ScMenuModal>
      <span className="background">
        ME
        <br />
        NU
      </span>
      <span className="background">
        WEB
        <br />
        KING
      </span>
      <nav>
        {navbar.map(({ transKey, link }, index) => (
          <a href={link} key={index}>
            <span>0{index + 1}</span>
            {t(`menu.${transKey}`)}
          </a>
        ))}
      </nav>
      <div className="media">
        <Button>
          <Fb />
        </Button>
        <Button>
          <Insta />
        </Button>
        <Button>
          <LinkedIn />
        </Button>
      </div>
    </ScMenuModal>
  );
};

export const ModalContacts: FC = () => {
  return (
    <ScContactsModal>
      <Button>
        <img src={wp} alt="" />
      </Button>
      <Button>
        <img src={tg} alt="" />
      </Button>
      <Button>
        <img src={viber} alt="" />
      </Button>
      <Button>
        <img src={mail} alt="" />
      </Button>
    </ScContactsModal>
  );
};

export const ModalForm: FC = () => {
  const [name, setName] = useState("");
  const [tel, setTel] = useState("");
  const [email, setEmail] = useState("");

  const { t } = useTranslation("layout");

  return (
    <form>
      <ScModalForm>
        <div className="headline">{t("modalForm.headline")}</div>
        <Input
          value={name}
          placeholder={
            <>
              {t("modalForm.name")}
              <span>*</span>
            </>
          }
          onChange={(e) => setName(e.target.value)}
        />
        <Input
          value={tel}
          placeholder={
            <>
              {t("modalForm.tel")}
              <span>*</span>
            </>
          }
          onChange={(e) => setTel(e.target.value)}
        />
        <Input
          value={email}
          placeholder={<>{t("modalForm.email")}</>}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Button colorTheme={ButtonTheme.Primary}>
          {t("modalForm.button")}
        </Button>
      </ScModalForm>
    </form>
  );
};
