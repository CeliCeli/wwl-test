import { createRoot } from "react-dom/client";
import i18n from "i18n";
import reportWebVitals from "reportWebVitals";
import { I18nextProvider } from "react-i18next";
import App from "./App";
import ErrorBoundary from "modules/Errors";

const container = document.getElementById("root");
const root = createRoot(container as Element);

root.render(
  <ErrorBoundary>
    <I18nextProvider i18n={i18n}>
      <App />
    </I18nextProvider>
  </ErrorBoundary>
);

reportWebVitals();
