import { createGlobalStyle } from "styled-components";
import { theme } from "../theme";

export const GlobalStyle = createGlobalStyle`

    @font-face {
      font-family: 'VisueltPro';
      src: url('/fonts/VisueltPro-Regular.ttf') format('truetype'),
          url('/fonts/VisueltPro-Regular.woff') format('woff'),
          url('/fonts/VisueltPro-Regular.woff2') format('woff2');
      font-weight: 400;
      font-style: normal;
    }
    @font-face {
      font-family: 'VisueltPro';
      src: url('/fonts/VisueltPro-Bold.ttf') format('truetype'),
          url('/fonts/VisueltPro-Bold.woff') format('woff'),
          url('/fonts/VisueltPro-Bold.woff2') format('woff2');
      font-weight: 700;
      font-style: normal;
    }
    @font-face {
      font-family: 'VisueltPro';
      src: url('/fonts/VisueltPro-Black.ttf') format('truetype'),
          url('/fonts/VisueltPro-Black.woff') format('woff'),
          url('/fonts/VisueltPro-Black.woff2') format('woff2');
      font-weight: 900;
      font-style: normal;
    }


    body{
      background-color: ${theme.color.secondary};
      color: ${theme.color.white};
      font-family: 'VisueltPro', sans-serif;
      font-weight: 400;
      margin: 0;
      &.is-open{
        overflow: hidden;
      }
    }
    a{
      text-decoration: none;
      transition: .2s;
      color:${theme.color.white};
      &:hover{
        color:${theme.color.primary}
      }
    }

    .cursor {
      position: fixed;
      width: 16px;
      height: 16px;
      border-radius: 50%;
      background-color: rgba(235, 56, 70, 0.3);
      pointer-events: none;
      z-index: 9999;
      transform: translate(-8px, -8px);
      transition: ease .1s;
    }

    h1{
      font-weight: 900;
      font-size: 58px;
      line-height: 150%;
      text-transform: uppercase;
      text-align: center;
      
      span{
        color: ${theme.color.primary};
      }
    }
`;
