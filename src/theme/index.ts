export const theme = {
  color: {
    primary: "#EB3846",
    secondary: "#171717",
    white: "#FFFFFF",
    gray: "#1B1B1B",
    lightGray: "#908F8F",
  },
  effect: {
    darkGradient: `linear-gradient(158.74deg, #171717 43.4%, #000000 113.07%);`,
    primaryGradient: `linear-gradient(320.66deg, #681217 14.75%, #BC333C 84.81%);`,
    primaryGradientInvet:
      "linear-gradient(320.66deg, #BC333C 14.75%, #681217 84.81%);",
    secondaryGraient:
      "linear-gradient(163.01deg, #181818 8.23%, #181818 91.98%), #949494;",
    secondaryGradientInvert:
      "linear-gradient(180deg, #282828 0%, #151515 100%);",
  },
};
